/**
 *  Почитай, найди время, по первой ссылке читай всё и делай упражнения, а потом можешь перейти к JS и заниматься там же.
 *  По 2-й ссылке общая информация, находи себе то, что интересно, и переходи по ссылкам
 * 
 * https://learn.javascript.ru/ui
 * https://frontendmasters.com/books/front-end-handbook/2019/
 * 
 */


// slider, i, fff -- все эти переменные и функция repeat, объявленные ниже, будут глобальной области видимости. Так делать не стоит, т.к.
// к этим переменным будет из других скриптов и из консоли, их могут переопределить, или ты можешь переопределить чужие переменные.
// всё это лучше перенести внутрь коллбэка (в этом файле внизу внутри $(function() { СЮДА }))
/*
// Бекграунд слайдер
let slider = document.getElementById('background_slider');
let i = 1;


// уверен, что хочешь менять картинки раз в 10000 секунд? Второй параметр -- это количество миллисекунд (интервал) между выховами функции.
// Выставишь 5000 и картинка будет меняться раз в 5 секунд
let fff = setInterval(repeat, 5000);

// также setInterval возвращает id, который дальше может использоватся в функции clearInterval, чтобы отменить остановить дальнейшее выполнение функции,
// которую передали в setInterval (в твоём случае repeat). А переменную, куда ты сохраняешь этот id, лучше назвать id или intervalId.
// И при переходе на другую страницу, если там уже не будет этого слайдера, ты сможешь отменить вызов функции repeat по интервалу.

// let intervalId = setInterval(repeat, 10000000);
// почитай https://learn.javascript.ru/settimeout-setinterval#setinterval

function repeat() {
    // document.getElementById возвращает DOM элемент или null, если элемента с таким id нет на странице. Переменная slider может быть null.
    // Так же переменная slider находится в глобальной области видимости и может быть переопределена.
    // Поэтому вставляем ещё эту проверку в if

    if (slider) {
        i++;
        switch (i) {
            case 1:
                slider.className = 'slider';
                break;
            case 2:
                slider.className = 'slider_2';
                break;
            case 3:
                slider.className = 'slider_3';
                i = 0;
                break;
        }
    }
}


// 2 следующих блока (Выезжающее меню и Выезжающий поиск) лучше вызывать точно зная, что DOM-дерево уже построено и готово к использованию
// в этом файле это $(function() { СЮДА })

//Выезжающее меню
document.querySelector('.menu_button__icon').addEventListener('click', () => {
    document.querySelector('.menu').classList.add('active');
    document.querySelector('.close-menu').classList.add('close-menu-active');
})
document.querySelector('.close-menu').addEventListener('click', () => {
    document.querySelector('.menu').classList.remove('active');
    document.querySelector('.close-menu').classList.remove('close-menu-active')
})


//Выезжающий поиск
document.querySelector('.search_button__icon').addEventListener('click', () => {
    document.querySelector('.search').classList.add('active');
    document.querySelector('.close-search').classList.add('close-search-active');
})
document.querySelector('.close-search').addEventListener('click', () => {
    document.querySelector('.search').classList.remove('active');
    document.querySelector('.close-search').classList.remove('close-search-active')
})


// вообще непонятно, зачем добавлялась jquery. Тем более что в некоторых местах используется DOM api напрямую,
// как в коде выше (document.querySelector, addEventListener(), classList.add, classList.remove),
// а потом вдруг для того же используется jquery $().
// Всё, что что сделано с помощью jquery, можно сделать и без него, тем более что там ниже почти всё можно удалить.
// А jquery - это лишняя зависимость, из-за которой страница будет медленнее грузиться.

// Строчка ниже коммента "Кнопка наверх" нужна для того, чтобы запускать код, когда DOM построен. Вместо неё можно использовать
// document.addEventListener("DOMContentLoaded", function() {});
// смотри https://learn.javascript.ru/onload-ondomcontentloaded
// Так что можно удалить из html подключение jquery и переписать код ниже. 

//Кнопка наверх
$(function() {
    // зачем строка ниже? На событие скролл добавляется event listener, куда передаётся пустая функция. Нужно или удалить,
    // или, если во время скролла задумывалось какое-то поведение, дописать внутрь function(){}
    $(window).scroll(function() {
    });

    // используется ли вообще кнопка наверх?
    // в той версии, что есть сейчас, скролла нет да и сама копка не видна
    // нужно определиться и потом либо поправить стили на кнопке, чтоб она была видна, либо удалить кнопку из html, её стили и код ниже
    $('#up_button').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });
});
*/

document.addEventListener("DOMContentLoaded", function() {
    let slider = document.getElementById('background_slider');
    let i = 1;

    setInterval(repeat, 5000);

    function repeat() {
        if (slider) {
            i++;

            slider.className = 'slider slider_' + i;

            if (i === 3) {
                i = 0;
            }
        }
    }

    //Выезжающее меню
    document.querySelector('.menu_button__icon').addEventListener('click', () => {
        document.querySelector('.menu').classList.add('active');
        document.querySelector('.close-menu').classList.add('close-menu-active');
    });
    document.querySelector('.close-menu').addEventListener('click', () => {
        document.querySelector('.menu').classList.remove('active');
        document.querySelector('.close-menu').classList.remove('close-menu-active')
    });


    //Выезжающий поиск
    document.querySelector('.search_button__icon').addEventListener('click', () => {
        document.querySelector('.search').classList.add('active');
        document.querySelector('.close-search').classList.add('close-search-active');

        // когда выезжает поиск, было бы круто поставить фокус на поле ввода. И чтобы по нажатию кнопки Esc это окно поиска закрывалось
    });
    document.querySelector('.close-search').addEventListener('click', () => {
        document.querySelector('.search').classList.remove('active');
        document.querySelector('.close-search').classList.remove('close-search-active')
    });
});