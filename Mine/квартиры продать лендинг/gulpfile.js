let postcss = require('gulp-postcss'), 
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('autoprefixer'),
    sourcemaps = require('gulp-sourcemaps');
 
sass.compiler = require('node-sass');
 
gulp.task('scss', function () {
  return gulp.src('project/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('project/css'))
    .pipe(browserSync.reload({stream: true}))
});


gulp.task('html', function(){
    return gulp.src('project/*.html')
    .pipe(browserSync.reload({stream: true}))
});
gulp.task('js', function(){
    return gulp.src('project/*.js')
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function(){
    browserSync.init({
        server: {
            baseDir: "project/"
        }
    });
});

gulp.task('watch', function () {
  gulp.watch('project/scss/**/*.scss', gulp.parallel('scss'));
  gulp.watch('project/*.html', gulp.parallel('html'));
});


gulp.task('default',gulp.parallel('scss', 'browser-sync', 'watch'));